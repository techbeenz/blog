@extends('layouts.app')
@section('content')
@section('page_title','Read The Articles')


<div class="jumbotron">
	<h2>  {{ strip_tags($article->title) }} </h2>  
	 <p>{{$article->authname}} Publish {{$article->created_at->diffForHumans()}}</p>
	@if(!Auth::guest()) 
		@if(Auth::user()->id == $article->authid)
			<form action="{{ route('articles.destroy', $article->id) }}" method="POST" onsubmit= "return confirm('Do you really delete this one?');">
            <input type="hidden" name="_method" value="DELETE"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
            <a class="btn btn-primary" href="{{  url('articles') }}/{{$article->id}}/edit">
				<i class="glyphicon glyphicon-edit"></i>
			</a>
            <button type="submit" class="delete btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </form>
		@endif
		@endif    
</div>

<div class="container">
	<div class="row">
		@if(empty($article->image))
		<div class="col-md-12">{!! nl2br(($article->body)) !!} </div>
		@else
		<div class="col-md-8">{!!nl2br(($article->body))!!} </div>
		<div class="col-md-4  text-center container-fluid">
			<img class="img-rounded img-responsive" src="{{URL::asset('/images/'.$article->image)}}">
		</div>
		@endif 
		
		
	</div>
</div> 
@endsection