@extends('layouts.app')
@section('content')
@section('page_title','Update The Article')

<form action={{route('articles.update',['id' => $article->id])}} method="POST" enctype="multipart/form-data">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input name="_method" type="hidden" value="PUT">

	<div class="jumbotron">
		<input name="title" type="text" class="form-control input-lg" id="title" placeholder="" value="{{ strip_tags($article->title)}}"></input>  
		<p>{{$article->authname}}</p>
		@if(!Auth::guest()) 
		@if(Auth::user()->id == $article->authid)
		<button type="submit" class="btn btn-success" ><i class="glyphicon glyphicon-ok"></i></button>
		<a class="btn btn-danger" href="{{route('articles.destroy',['id' => $article->id])}}">
			<i class="glyphicon glyphicon-remove"></i>
		</a> 
		@endif
		@endif    
	</div>

	<div class="container">
		<div class="row">

			<div class="col-md-8">
			<textarea name="body" class="form-control" id="body" rows="20">{{strip_tags($article->body) }}</textarea> 
			</div>
			<div class="col-md-4  text-center container-fluid">
				<img id="articleimg" class="img-rounded img-responsive" src="{{empty($article->image)?"/images/default_product.png":URL::asset('/images/'.$article->image)}}"></br></br>
				<span class="btn btn-primary text-center" onclick="btn_select_file()">Browse Image</span>
				<input id="file" name="file" type="file" style="display: none;"  onchange="onFileSelected()"" accept="image/*" >
			</div>
		</div>
	</div> 
</form>
<script type="text/javascript" src="/js/articles.js"></script>
@endsection