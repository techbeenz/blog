@extends('layouts.app')
@section('content')
@section('page_title','Home')


@foreach ($articles as $art)

<div class="panel panel-default">
  <div class="panel-heading list-group-item-heading">
   
      <div class="row">
        <div class="col-md-11 col-xs-11"> <a href="{{url('articles')}}/{{$art->id}}"><h4><strong>{{ strip_tags($art->title)}}</strong></h4></a></div>
        @if(!Auth::guest()) 
          @if(Auth::user()->id == $art->authid) 
            <form action="{{ route('articles.destroy', $art->id) }}" method="POST" onsubmit= "return confirm('Do you really delete this one?');">
            <input type="hidden" name="_method" value="DELETE"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" /> 
            <a class="btn btn btn-primary" href="{{route('articles.edit',['id' => $art->id])}}">
               <i class="glyphicon glyphicon-edit"></i>
            </a>
            <button type="submit" class="delete btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </form>
          @endif
        @endif
      </div>
  </div>
<div class="panel-body">
  {{substr(strip_tags($art->body),0,500)}}{{strlen($art->body)>500 ? '...':''}}
</div>
<div class="panel-footer">
   {{$art->authname}} Publish {{$art->created_at->diffForHumans()}} 
</div>
</div>
<script type="text/javascript" src="/js/articles.js"></script>
@endforeach
@endsection