@extends('layouts.app')
@section('content')
@section('page_title','Create New One')

 

<form action={{url('articles')}} method="post" enctype="multipart/form-data">

	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label for="title">Title:</label>
				<input name="title" type="text" class="form-control" id="title" placeholder="">
			</div> 

			<div class="form-group">
				<label for="body">Body</label>
				<textarea name="body" class="form-control" id="body" rows="20"></textarea>
			</div>

		</div>

		<div class="col-md-4  text-center container-fluid"></br></br></br>
			<div class="form-group"> 
				<img id="articleimg" src="/images/default_product.png" class="img-thumbnail">
				</br></br>
				<span class="btn btn-primary text-center" onclick="btn_select_file()"">Browse Image</span>
			</div>
		</div>

		<div class="col-md-12">
			<button type="submit" class="btn btn-success" >Submit</button>
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			<input id="file" name="file" type="file" style="display: none;"  onchange="onFileSelected()"" accept="image/*" >
		</div>
	</div>
</form>

 
        <script type="text/javascript" src="/js/articles.js"></script>


        @endsection