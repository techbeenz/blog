<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Article;
use Image;
use Purifier;
use Auth;
use Session;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all()->sortByDesc("created_at");;
        return view('articles.index')->withArticles($articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


     $this->validate($request, array(
        'title'         => 'required|max:255',
        'body'          => 'required',
        'file' => 'max:2048|mimes:jpeg,png' 
        ));
      // store in the database




     $art = new Article();
     $art->title = Purifier::clean($request->title);  
     $art->body = Purifier::clean($request->body);
     $art->authname = Auth::user()->name;
     $art->authid = Auth::user()->id;
     if ($request->hasFile('file')) {
        $image = $request->file('file');
        $filename =date('Ymd').time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('images/'); 
        $request->file('file')->move($location,$filename);
        $art->image = $filename;
    }
    else
    {
        $art->image = '';
    }
    $art->save(); 
        //$post->tags()->sync($request->tags, false);
    Session::flash('success', 'The article was successfully save!');
    return redirect()->route('articles.show', $art->id); 

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        
        return view('articles.show')->withArticle($article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        return view('articles.update')->withArticle($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     $this->validate($request, array(
        'title'         => 'required|max:255',
        'body'          => 'required',
        'file' => 'max:2048|mimes:jpeg,png' 
        ));
  

     $art = Article::find($id);
     $art->title = Purifier::clean($request->title);  
     $art->body = Purifier::clean($request->body);
     $art->authname = Auth::user()->name;
     $art->authid = Auth::user()->id;
 

     if ($request->hasFile('file')) 
     {
        $image = $request->file('file');
        $filename =date('Ymd').time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('images/'); 
        $request->file('file')->move($location,$filename);
        $art->image = $filename;
    }
    $art->save(); 
        //$post->tags()->sync($request->tags, false);
    Session::flash('success', 'The article was successfully save!');
    return redirect()->route('articles.show', $art->id); 
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $art = Article::find($id);
        $art->delete(); 
        return redirect()->route('articles.index'); 
    }
}
